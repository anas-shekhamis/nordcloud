# Solution

## Dependencies
I used Node.js `v8.10.0` and Yarn.

## How to Start
The starting point of the app is `index.js`.

### Install Packages
```
yarn install
```

### Run the App
```
yarn start
```

### To Run the Tests
```
yarn test
```