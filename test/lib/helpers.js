/* global describe it */

require('mocha');
const assert = require('assert');
const { calculateDistanceBetweenPoints } = require('../../lib/helpers');

describe('helpers', () => {
  describe('#calculateDistanceBetweenPoints', () => {
    it('should return 0 when the passed points are (0, 0) and (0, 0)', () => {
      const point1 = { x: 0, y: 0 };
      const point2 = { x: 0, y: 0 };

      assert.equal(calculateDistanceBetweenPoints(point1, point2), 0);
    });

    it('should return 25 when the passed points are (15, 20) and (35, 5)', () => {
      const point1 = { x: 15, y: 20 };
      const point2 = { x: 35, y: 5 };

      assert.equal(calculateDistanceBetweenPoints(point1, point2), 25);
    });

    it('should return NaN when the passed points are empty objects', () => {
      assert.ok(Number.isNaN(calculateDistanceBetweenPoints({}, {})));
    });
  });
});
