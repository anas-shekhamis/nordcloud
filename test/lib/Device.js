/* global describe it */

require('mocha');
const assert = require('assert');
const { validateDevice } = require('../../lib/Device');

describe('Device', () => {
  describe('#validateDevice', () => {
    it('should return true whe the device coordinates are (5, 10)', () => {
      const device = { x: 5, y: 10 };
      assert.ok(validateDevice(device));
    });

    it('should return false whe the device coordinates are (false, 10)', () => {
      const device = { x: false, y: 10 };
      assert.equal(validateDevice(device), false);
    });
  });
});
