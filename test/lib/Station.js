/* global describe it */

require('mocha');
const assert = require('assert');
const { findMostSuitableStationForDevice } = require('../../lib/Station');

describe('Station', () => {
  describe('#findMostSuitableStationForDevice', () => {
    it('should return station(0, 0, 10) with power 100 when the device coordinates are (0, 0)', () => {
      const device = { x: 0, y: 0 };
      const stations = [
        { location: { x: 0, y: 0 }, reach: 10 },
        { location: { x: 20, y: 20 }, reach: 5 },
        { location: { x: 10, y: 0 }, reach: 12 },
      ];
      const expectedStation = { ...stations[0], power: 100 };

      assert.deepStrictEqual(findMostSuitableStationForDevice(stations, device), expectedStation);
    });

    it('should return station(0, 0, 10) with power 100 when the device coordinates are (0, 0)', () => {
      const device = { x: 15, y: 10 };
      const stations = [
        { location: { x: 0, y: 0 }, reach: 10 },
        { location: { x: 20, y: 20 }, reach: 5 },
        { location: { x: 10, y: 0 }, reach: 12 },
      ];
      const expectedStation = { ...stations[2], power: 0.6718427000252355 };

      assert.deepStrictEqual(findMostSuitableStationForDevice(stations, device), expectedStation);
    });

    it('should return station(0, 0, 10) with power 100 when the device coordinates are (0, 0)', () => {
      const device = { x: 100, y: 100 };
      const stations = [
        { location: { x: 0, y: 0 }, reach: 10 },
        { location: { x: 20, y: 20 }, reach: 5 },
        { location: { x: 10, y: 0 }, reach: 12 },
      ];

      assert.equal(findMostSuitableStationForDevice(stations, device), undefined);
    });

    it('should throw an error when a station is not valid', () => {
      const device = { x: 15, y: 10 };
      const stations = [
        { location: { x: 0, y: 0 }, reach: 'some wrong value' },
        { location: { x: 20, y: 20 }, reach: 5 },
        { location: { x: 10, y: 0 }, reach: 12 },
      ];

      assert.throws(() => { findMostSuitableStationForDevice(stations, device); }, 'Not valid station - station(0, 0, some wrong value)');
    });
  });
});
