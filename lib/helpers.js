/**
 * Calculate the distance between two points in 2-dimensional space
 * @param {object} point1
 * @param {object} point2
 * @returns {number} distance between point1 and point2
 */
exports.calculateDistanceBetweenPoints = (point1, point2) => Math.sqrt(
  (Math.abs(point1.x - point2.x) ** 2) + (Math.abs(point1.y - point2.y) ** 2),
);

/**
 * Print/Output function
 * @param {string} message
 */
exports.printMessage = (message) => {
  console.log(message);
};
