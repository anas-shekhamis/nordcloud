const isNumber = require('lodash/isNumber');
const { calculateDistanceBetweenPoints } = require('./helpers');

/**
 * Calculate the station power given the reach of the station
 * and the distance between the device and the station
 *
 * @param distance between the device and the station
 * @param reach of the station
 * @returns {number} station power
 */
const calculatePower = (distance, reach) => (distance > reach ? 0 : ((reach - distance) ** 2));

/**
 * Check validity of station object
 * @param {Object} station
 * @returns {boolean}
 */
const validateStation = (station) => {
  if (
    isNumber(station.location.x)
    && isNumber(station.location.y)
    && isNumber(station.reach)
  ) return true;

  return false;
};

/**
 * Locate the most suitable station for a given device
 * @param {Array} stations
 * @param {Object} device
 * @returns {Object|undefined} most suitable (with most power) station or undefined
 * @throws will throw an error if one of the station object was not valid
 */
exports.findMostSuitableStationForDevice = (stations, device) => stations
  .map((station) => {
    if (!validateStation(station)) {
      throw Error(`Not valid station - station(${station.location.x}, ${station.location.y}, ${station.reach})`);
    }

    const tempStation = station;
    const distance = calculateDistanceBetweenPoints(device, station.location);
    tempStation.power = calculatePower(distance, station.reach);
    return tempStation;
  })
  .filter(station => station.power > 0)
  .sort((station1, station2) => station1.power > station2.power)
  .pop();
