const isNumber = require('lodash/isNumber');

/**
 * Check validity of device object
 * @param {Object} device
 * @returns {boolean}
 */
exports.validateDevice = (device) => {
  if (
    isNumber(device.x)
    && isNumber(device.y)
  ) return true;

  return false;
};
