const { printMessage } = require('./lib/helpers');
const { findMostSuitableStationForDevice } = require('./lib/Station');
const { validateDevice } = require('./lib/Device');

// initiating stations array
const stations = [
  { location: { x: 0, y: 0 }, reach: 10 },
  { location: { x: 20, y: 20 }, reach: 5 },
  { location: { x: 10, y: 0 }, reach: 12 },
];

// initiating devices array
const devices = [
  { x: 0, y: 0 },
  { x: 100, y: 100 },
  { x: 15, y: 10 },
  { x: 18, y: 18 },
];

try {
  // finding most suitable station for each device
  devices.forEach((device) => {
    if (!validateDevice(device)) {
      throw Error(`Not valid device - device(${device.x}, ${device.y})`);
    }

    const station = findMostSuitableStationForDevice(stations, device);

    if (!station) {
      printMessage(`No link station within reach for point ${device.x}, ${device.y}`);
    } else {
      printMessage(`Best link station for point ${device.x}, ${device.y} is ${station.location.x}, ${station.location.y} with power ${station.power.toFixed(2)}`);
    }
  });
} catch (err) {
  printMessage(err);
}
